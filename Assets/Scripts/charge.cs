using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class charge : MonoBehaviour
{

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<ArticulationBody>() != null)
        {
            FixedJoint joint = this.gameObject.AddComponent<FixedJoint>();
            joint.connectedArticulationBody = collision.articulationBody;

            // Affiche un message dans la console pour confirmer que le joint est cr��
            Debug.Log("Collision d�tect�e et FixedJoint ajout�.");
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            Destroy(this.gameObject.GetComponent<FixedJoint>());

            // Affiche un message dans la console pour confirmer la destruction du joint
            Debug.Log("FixedJoint d�truit.");
        }
    }
}
