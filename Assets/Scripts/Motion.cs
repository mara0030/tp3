using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motion : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Update()
    {
        GérerMouvement();
        GérerRotation();
    }

    void GérerMouvement()
    {
        float vitesse = 0.01f;

        if (Input.GetKey(KeyCode.DownArrow))
        {
            Déplacer(Vector3.forward * vitesse);
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            Déplacer(Vector3.back * vitesse);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            Déplacer(Vector3.left * vitesse);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            Déplacer(Vector3.right * vitesse);
        }
    }

    void Déplacer(Vector3 direction)
    {
        transform.Translate(direction, Space.World);
    }

    void GérerRotation()
    {

        if (Input.GetKey(KeyCode.A))
        {
            transform.Rotate(Vector3.forward, -2);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Rotate(Vector3.forward, 2);
        }
    }

}
